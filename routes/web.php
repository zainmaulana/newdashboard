<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('dashboard');
});

Auth::routes();

Route::group(['middleware' => ['auth']], function () {

    Route::get('/home', 'HomeController@index')->name('home');

    Route::post('/change_password','DashboardController@change_pass_user');

    //Dashboard ERP
    Route::get('/', 'DashboardController@index')->name('dashboard');
    Route::get('/dashboard/dsu', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/epc', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/dg', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/pjt', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/opt', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/rjt', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/hki', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/hka', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/hkr', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/bhirawa', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/sis', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/petronesia', 'DashboardController@dashboard_auto');
    Route::get('/dashboard/jakon','DashboardController@dashboard_auto');
	Route::get('/dashboard/acp','DashboardController@dashboard_auto');
    Route::get('/dashboard/npp','DashboardController@dashboard_auto');
	Route::get('/dashboard/cp','DashboardController@dashboard_auto');
	Route::get('/dashboard/ak','DashboardController@dashboard_auto');
	Route::get('/dashboard/kpbu','DashboardController@dashboard_auto');
	Route::get('/dashboard/legal','DashboardController@dashboard_auto');
	Route::get('/dashboard/manrisk','DashboardController@dashboard_auto');
	Route::get('/dashboard/qhsse','DashboardController@dashboard_auto');
	Route::get('/dashboard/sekper','DashboardController@dashboard_auto');
	Route::get('/dashboard/sit','DashboardController@dashboard_auto');
	Route::get('/dashboard/spi','DashboardController@dashboard_auto');
	Route::get('/dashboard/epcenergy','DashboardController@dashboard_auto');
	Route::get('/dashboard/others','DashboardController@dashboard_auto');
	Route::get('/dashboard/rapat','DashboardController@dashboard_auto');

    //Dashboard Non ERP
	Route::get('/dashboard/pbi','DashboardController@dashboard_auto');
	Route::get('/dashboard/hc','DashboardController@dashboard_auto');

    //CCTV
    Route::get('/cctv', 'DashboardController@cctv_index')->name('cctv_home');
    Route::get('/cctv/dsu', 'DashboardController@cctv_auto');

    //MD - User
    Route::get('/masterdata/user', 'MasterDataController@list_user')->name('user');
    Route::post('/masterdata/user/insert','MasterDataController@insert_user');
    Route::post('/masterdata/user/update{id}','MasterDataController@update_user');
    Route::post('/masterdata/user/delete{id}', 'MasterDataController@delete_user');

    //MD - Divisi
    Route::get('/masterdata/divisi', 'MasterDataController@list_divisi')->name('divisi');
    Route::post('/masterdata/divisi/insert','MasterDataController@insert_divisi');
    Route::post('/masterdata/divisi/update{id}','MasterDataController@update_divisi');
    Route::post('/masterdata/divisi/delete{id}', 'MasterDataController@delete_divisi');

    //MD - Permission
    Route::get('/masterdata/permission', 'MasterDataController@list_permission')->name('permission');
    Route::post('/masterdata/permission/insert','MasterDataController@insert_permission');
    Route::post('/masterdata/permission/update{id}','MasterDataController@update_permission');
    Route::post('/masterdata/permission/delete{id}', 'MasterDataController@delete_permission');

    //MD - Role
    Route::get('/masterdata/role', 'MasterDataController@list_role')->name('role');
    Route::post('/masterdata/role/insert','MasterDataController@insert_role');
    Route::post('/masterdata/role/update{id}','MasterDataController@update_role');
    Route::post('/masterdata/role/delete{id}', 'MasterDataController@delete_role');
});
