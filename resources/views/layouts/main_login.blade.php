<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Favicon -->
		<link rel="icon" type="image/x-icon" href="../assets/images/brand/hk_logo.png"/>
		<link rel="shortcut icon" type="image/x-icon" href="../assets/images/brand/hk_logo.png" />

		<!-- Title -->
		<title> Dashboard Hutama Karya</title>

        <!-- Global stylesheets -->
        @include('includes.main_css')
        <!-- /global stylesheets -->
    </head>

    <body style="
    background-image: url('../images/banners/login_bg.png');
    background-repeat: no-repeat;
    background-attachment: fixed;
    background-size: cover;
    ">

        <!-- Page content -->
        @include('includes.flash-message')
        @yield('content')
        <!-- /page content -->

        <!-- Global js -->
        @include('includes.main_js')
        <!-- /global js -->

    </body>
</html>