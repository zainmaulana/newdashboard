<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Meta data -->
		<meta charset="UTF-8">
		<meta name='viewport' content='width=device-width, initial-scale=1.0, user-scalable=0'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">

		<!-- Favicon -->
		<link rel="icon" type="image/x-icon" href="../assets/images/brand/hk_logo.png"/>
		<link rel="shortcut icon" type="image/x-icon" href="../assets/images/brand/hk_logo.png" />

		<!-- Title -->
		<title> Dashboard Hutama Karya</title>

        <!-- Global stylesheets -->
        <link href="../css/jquery.passwordRequirements.css" rel="stylesheet">
        @include('includes.main_css')
        <!-- /global stylesheets -->

        <!-- JQuery js-->
        <script src="../js/jquery-3.2.1.min.js"></script>

        <!-- Bootstrap js -->
        <script src="../plugins/bootstrap-4.3.1/js/popper.min.js"></script>
        <script src="../plugins/bootstrap-4.3.1/js/bootstrap.min.js"></script>

        <script src="../js/jquery.passwordRequirements.js"></script>
    </head>

    <body>

        <!-- Top Section -->
		<div class="banner1 relative banner-top">

            <!-- Main navbar -->
            @include('features.navbar')
            <!-- /main navbar -->

            <!-- Page header -->
            @include('features.pageheader')
            <!-- /page header -->
		</div>
        <!-- / top section -->

        <!-- Page content -->
        @include('includes.flash-message')
        @yield('content')
        <!-- /page content -->

        <!-- Footer -->
        @include('features.footer')
        <!-- /footer -->

        <!-- Back to top -->
        <a href="#top" id="back-to-top" ><i class="fa fa-long-arrow-up"></i></a>

        <!-- Global js -->
        @include('includes.main_js')
        <!-- /global js -->

    </body>
</html>