<!-- Carousel -->
<div class="owl-carousel bannner-owl-carousel slider slider-header">
	<div class="item cover-image" data-image-src="">
		<img alt="first slide" src="../images/banners/hk_proyek_1.jpg">
	</div>
	<div class="item">
		<img alt="first slide" src="../images/banners/hk_proyek_2.jpg">
	</div>
	<div class="item">
		<img alt="first slide" src="../images/banners/hk_proyek_3.jpg">
	</div>
</div><!--/Carousel-->

<!--Section-->
<section>
	<div class="slider-images">
		<div class="container-fuild">
			<div class="header-text slide-header-text mt-0 mb-0">
				<div class="col-xl-6 col-lg-6 col-md-6 d-block" style="margin-left: 180px;">
					<div class="search-background bg-transparent input-field">
						<div class="mb-6">
							<h2 class="mb-1 d-none d-md-block" style="color: #2C388C;">{{ $greet ?? '' }}, {{ Auth::user()->name }} </h2>
							<h2 class="mb-1 d-none d-md-block" style="color: #2C388C;"><b>Welcome to CCTV Portal inside HK Idea</b></h2>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section><!--/Section-->