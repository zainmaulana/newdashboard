<?php
    use Illuminate\Support\Facades\DB;

    $iduser = Auth::user()->id;

        //Nama User
        $user = DB::table('users as a')
        ->leftJoin('roles as b','a.id_role','=','b.id')
        ->leftJoin('tblm_divisi as c','a.id_divisi','=','c.id_divisi')
    	->select('a.*',
        'b.id as id_role',
    	'b.name as nama_role',
        'c.*')
		->where('a.id','=',$iduser)
        ->orderBy('name','ASC')
        ->first();
?>

<div class="header-main">
	<!--Header-->
	<header class="header-search header-logosec p-2" style="height: 50px; background-color: #2C388C;">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-12">
				</div>
			</div>
		</div>
	</header><!--/Header-->

	<!-- Mobile Header -->
	<div class="horizontal-header clearfix ">
		<div class="container">
			<a id="horizontal-navtoggle" class="animated-arrow"><span></span></a>
			<span class="smllogo"><img src="../images/brand/bumn_logo.png" width="auto" height="20px" alt="img"/></span>
			<span class="smllogo"><img src="../images/brand/hk_logo.png" width="auto" height="30px" alt="img"/></span>
		    <!-- <span class="smllogo-white"><img src="../images/brand/logo.png" width="120" alt="img"/></span> -->
		    <a href="{{ route('logout') }}" class="callusbtn" onclick="event.preventDefault();
											document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i></a>
		</div>
	</div><!-- /Mobile Header -->

	<!--Horizontal-main -->
	<div class="header-style horizontal-main bg-white clearfix">
		<div class="horizontal-mainwrapper container clearfix">
			<nav class="horizontalMenu clearfix d-md-flex">
				<ul class="horizontalMenu-list">
					<!-- <li aria-haspopup="true">
						<img src="../images/brand/bumn_logo.png" width="auto" height="30px" class="header-brand-img" alt=" logo">
						<img src="../images/brand/hk_logo.png" width="auto" height="50px" class="header-brand-img" alt=" logo">
					</li> -->
					<li aria-haspopup="true">
						<a href="{{ route('dashboard') }}" style="color: #353434; font-size: larger;">Home </a>
					</li>
					@if($user->nama_role == "Super Admin")
					<li aria-haspopup="true"><a href="#" style="color: #353434; font-size: larger;">About Us </a></li>
					<li aria-haspopup="true"><a href="#" style="color: #353434; font-size: larger;">Master Data <span class="fe fe-chevron-down"></span></a>
						<ul class="sub-menu">
							<li aria-haspopup="true"><a href="{{ route('user') }}">User</a></li>
							<li aria-haspopup="true"><a href="{{ route('divisi') }}">Divisi</a></li>
							<li aria-haspopup="true"><a href="{{ route('permission') }}">Permission</a></li>
							<li aria-haspopup="true"><a href="{{ route('role') }}">Role</a></li>
						</ul>
					</li>
					@endif
					<li aria-haspopup="true">
						<a href="{{ route('cctv_home') }}" style="color: #353434; font-size: larger;">CCTV </a>
					</li>
				</ul>
				<ul class="mb-0">
					<li aria-haspopup="true" class="d-none d-lg-block">
						<span>
							<a class="btn btn-danger ad-post" href="{{ route('logout') }}"
							onclick="event.preventDefault();
											document.getElementById('logout-form').submit();">
								Logout
							</a>
						</span>
					</li>

					<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        @csrf
                    </form>
				</ul>
			</nav>
		</div>
	</div>
</div><!--/Horizontal-main -->