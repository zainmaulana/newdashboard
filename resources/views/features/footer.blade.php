<!--Footer Section-->
<section>
	<footer class="bg-dark text-white">
		<div class="footer-main">
			<div class="container">
				<div class="row">
					<div class="col-lg-3 col-md-12">
						<h6>About</h6>
						<hr class="deep-purple accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<p>Hutama Karya merupakan Badan Usaha Milik Negara (BUMN)  yang bergerak dibidang jasa konstruksi, pengembang dan penyedia jasa jalan tol.</p>
						<p class="mb-0">Kegiatan usaha yang bergerak dibidang Investasi, Properti & Realti, Industri & EPC, serta Infrastruktur.</p>
					</div>
					<div class="col-lg-3 col-md-12">
						<h6>Contact</h6>
						<hr class="deep-purple text-primary accent-2 mb-4 mt-0 d-inline-block mx-auto">
						<ul class="list-unstyled mb-0">
							<li>
								<a href="https://www.hutamakarya.com/"><i class="fa fa-home mr-3 text-primary"></i> PT. Hutama Karya (Persero)</a>
							</li>
							<li>
								<a href="#"><i class="fa fa-envelope mr-3 text-primary"></i> dashboard.hk@hutamakarya.com</a>
                            </li>
							<!-- <li>
								<a href="#"><i class="fa fa-phone mr-3 text-primary"></i> + 01 234 567 88</a>
							</li> -->
							<!-- <li>
								<a href="#"><i class="fa fa-print mr-3 text-primary"></i> + 01 234 567 89</a>
							</li> -->
						</ul>
						<!-- <ul class="list-unstyled list-inline mt-3">
							<li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
								    <i class="fa fa-facebook"></i>
								</a>
							</li>
                            <li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
								    <i class="fa fa-facebook"></i>
								</a>
							</li>
                            <li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
								    <i class="fa fa-facebook"></i>
								</a>
							</li>
                            <li class="list-inline-item">
								<a class="btn-floating btn-sm rgba-white-slight mx-1 waves-effect waves-light">
								    <i class="fa fa-facebook"></i>
								</a>
							</li>
						</ul> -->
					</div>
				</div>
			</div>
		</div>
		<div class="bg-dark text-white p-0">
			<div class="container">
				<div class="row d-flex">
					<div class="col-lg-12 col-sm-12 mt-3 mb-3 text-center ">
					    Copyright © 2021 PT Hutama Karya (Persero). Divisi Corporate Planning & Divisi Sistem, IT & Riset Teknologi All rights reserved.
					</div>
				</div>
			</div>
			<img src="../images/png/rec_12.png" width="100%" height="50%">
		</div>
	</footer>
</section><!--/Footer Section-->