@extends('layouts.main_template2')
@section('content')
    <!--Section-->
    <section class="sptb bg-white">
        <div class="container">
            <div class="section-title center-block text-center">
                <h2>Entitas Hutama Karya</h2>
				<img src="../images/png/rec_13.png" width="100%" height="100%">
            </div>
			<div class="section-title center-block text-center">
                <h2>Entitas Hutama Karya</h2>
				<img src="../images/png/rec_13.png" width="100%" height="100%">
            </div>
            <div class="item-all-cat center-block text-center education-categories">
                <div class="row d-flex justify-content-center">
					@foreach($userdivisiCCTV as $u)
                    <div class="col-lg-2 col-md-4 col-sm-6">
                        <div class="item-all-card text-dark text-center" style="height: 180px; border-color: #2C388C;">
							@if($u->kode_divisi == 'KONSOL')
                            	<a href="{{ $u->url_divisi }}" target="_blank"></a>
							@else
								<a href="{{ $u->url_divisi }}"></a>
							@endif
                            <img src="../images/entitas/{{ $u->image_divisi }}" width="100" height="100">
                            <div class="item-all-text mt-3">
								<?php
									$nama_divisi = explode('_',$u->nama_divisi);
								?>
                                <h5 class="mb-0">{{ $nama_divisi[1] }}</h5>
                            </div>
                        </div>
                    </div>
					@endforeach
                </div>
            </div>
        </div>
    </section><!--/Section-->

	<!--Section-->
	@if($routename <> '//')
	<section class="sptb">
		<div class="container">
			<div class="section-title center-block text-center">
				<h2>{{ $judul }}</h2>
				<img src="../images/png/rec_32.png" width="100%" height="100%">
			</div>
			<div class="panel panel-primary">
				<div>
					<div class="tabs-menu ">
						<!-- Tabs -->
						<ul class="nav panel-tabs eductaional-tabs mb-6">
							<?php $n1 = 1?>
							@foreach($listCCTV as $u)
							@if($n1 == 1)
							<li style="margin-bottom: 30px;"><a href="#tab{{ $n1++ }}" class="active show" data-toggle="tab">{{ $u->nama_report }}</a></li>
							@else
							<li style="margin-bottom: 30px;"><a href="#tab{{ $n1++ }}" data-toggle="tab">{{ $u->nama_report }}</a></li>
							@endif
							@endforeach
						</ul>
					</div>
				</div>
				<div class="panel-body">
					<div class="tab-content">
						<?php $n2 = 1?>
						@foreach($listCCTV as $u)
						@if($n2 == 1)
						<div class="tab-pane active show" id="tab{{ $n2++ }}">
							<div class="row">
								<div class="col-xl-12 col-md-12">
									<div class="card overflow-hidden">
										<!-- <div class="ribbon ribbon-top-left text-danger"><span class="bg-danger">Free</span></div> -->
										<div class="card-body">
											<div class="item-card7-desc">
												<div class="item-card7-text">
													<h3 class="font-weight-semibold">{{ $u->judul_report }}</h3>
												</div>
												<!-- <ul class="d-flex mb-2">
													<li class=""><a href="#" class="icons text-muted"><i class="icon icon-location-pin  mr-1"></i> USA</a></li>
													<li><a href="#" class="icons text-muted"><i class="icon icon-event  mr-1"></i>1 min ago</a></li>
													<li class=""><a href="#" class="icons text-muted"><i class="icon icon-phone  mr-1"></i> 14 675 65</a></li>
												</ul> -->
												<p class="mb-0">{{ $u->desc_report }}</p>
											</div>
										</div>
										<div class="row">
											<?php
												$iframes = explode('|',$u->iframe_report);
												$n3 = 1;

												$count_iframes = count($iframes);
											?>
											@foreach($iframes as $cam_cctv)
												<div class="col-md-6">
													<video id="{{ $n3++ }}" class="video-js vjs-default-skin" controls preload="auto" width="1150" height="650" data-setup='{}'>
														<source src="{{ $cam_cctv }}" type="application/x-mpegURL">
													</video>
												</div>
											@endforeach
										</div>
										<!-- <div class="item-card7-img">
											<div class="item-card7-imgs"> -->
												<!-- <a href="education.html"></a> -->
												<!-- <img src="../images/media/pictures/12.jpg" alt="img" class="cover-image"> -->
                                                <!-- <iframe width="100%" height="650" src="{{ $u->iframe_report }}" frameborder="0" allowFullScreen="true"></iframe> -->
											<!-- </div> -->
											<!-- <div class="item-card7-overlaytext">
												<a href="education.html" class="text-white"> Marketing</a>
											</div> -->
										<!-- </div> -->
										<!-- <div class="card-body p-4 pl-5">
											<a class="mr-4"><span class="font-weight-bold">Duration :</span> <span class="text-muted">6 Months</span></a>
											<a class="mr-4 float-right"><span class="font-weight-bold">Daily :</span><span class="text-muted"> 2 Hours </span></a>
										</div>
										<div class="card-body">
											<a href="education.html" class="btn btn-primary btn-block">Join Free</a>
										</div> -->
									</div>
								</div>
							</div>
						</div>
						@else
						<div class="tab-pane" id="tab{{ $n2++ }}">
                            <div class="row">
								<div class="col-xl-12 col-md-12">
									<div class="card overflow-hidden">
										<!-- <div class="ribbon ribbon-top-left text-danger"><span class="bg-danger">Free</span></div> -->
										<div class="card-body">
											<div class="item-card7-desc">
												<div class="item-card7-text">
													<h3 class="font-weight-semibold">{{ $u->judul_report }}</h3>
												</div>
												<!-- <ul class="d-flex mb-2">
													<li class=""><a href="#" class="icons text-muted"><i class="icon icon-location-pin  mr-1"></i> USA</a></li>
													<li><a href="#" class="icons text-muted"><i class="icon icon-event  mr-1"></i>1 min ago</a></li>
													<li class=""><a href="#" class="icons text-muted"><i class="icon icon-phone  mr-1"></i> 14 675 65</a></li>
												</ul> -->
												<p class="mb-0">{{ $u->desc_report }}</p>
											</div>
										</div>
										<!-- <div class="item-card7-img">
											<div class="item-card7-imgs"> -->
												<!-- <a href="education.html"></a> -->
												<!-- <img src="../images/media/pictures/12.jpg" alt="img" class="cover-image"> -->
                                                <!-- <iframe width="100%" height="650" src="{{ $u->iframe_report }}" frameborder="0" allowFullScreen="true"></iframe> -->
												<video id="cctv_video" class="video-js vjs-default-skin" controls preload="auto" width="1150" height="650" data-setup='{}'>
													<source src="{{ $u->iframe_report }}" type="application/x-mpegURL">
												</video>
											<!-- </div> -->
											<!-- <div class="item-card7-overlaytext">
												<a href="education.html" class="text-white"> Marketing</a>
											</div> -->
										<!-- </div> -->
										<!-- <div class="card-body p-4 pl-5">
											<a class="mr-4"><span class="font-weight-bold">Duration :</span> <span class="text-muted">6 Months</span></a>
											<a class="mr-4 float-right"><span class="font-weight-bold">Daily :</span><span class="text-muted"> 2 Hours </span></a>
										</div>
										<div class="card-body">
											<a href="education.html" class="btn btn-primary btn-block">Join Free</a>
										</div> -->
									</div>
								</div>
							</div>
						</div>
						@endif
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</section>
	@endif
	<!--/Section-->

	<script type="text/javascript">
		//Autoplay Video
		var c_iframes = <?php echo(json_encode($count_iframes)); ?>;

		for (var i = 1; i <= c_iframes; i++) {
			var video = document.getElementById(i);

			setTimeout(pauseVid, 5000);

			function play() {
				window.setTimeout(pauseVid, 5000);
			}

			function pauseVid() {
				video.pause();
				window.setTimeout(play, 5000);
			}
		}
	</script>
@endsection