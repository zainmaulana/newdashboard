@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-white bg-primary mb-3">
                    <h5 class="card-title">Master Data Divisi</h5>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#addData">
                            <i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Data</button>
                        </div>
                        <div class="col-md-12">
                            <br>
                        </div>
                        <div class="col-md-12">
                            <table id="table_id" class="table table-striped table-bordered nowrap" style="width: 100%;">
                                <thead>
                                    <tr>
                                        <th>No.</th>
                                        <th>Nama Divisi</th>
                                        <th>Kode Divisi</th>
                                        <th>Lantai Divisi</th>
                                        <th>Logo Divisi</th>
                                        <th>URL Divisi</th>
                                        <th>No. Urut Divisi</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $n = 1?>
                                    @foreach($divisi as $p)
                                    <tr>
                                        <td>{{ $n++ }}</td>
                                        <td>{{ $p->nama_divisi }}</td>
                                        <td>{{ $p->kode_divisi }}</td>
                                        <td>{{ $p->lantai_divisi }}</td>
                                        <td>
                                            <img width="100px" width="100px" src="{{ url('/images/entitas/'.$p->image_divisi) }}">
                                        </td>
                                        <td>{{ $p->url_divisi }}</td>
                                        <td>{{ $p->no_urut_divisi }}</td>
                                        <td>
                                            <center>
                                                <button type="button" class="btn btn-primary" data-toggle="modal"
                                                data-target="#editData_Update{{ $p->id_divisi }}">
                                                <i class="fa fa-pencil" aria-hidden="true"></i></button>
                                                <button type="button" class="btn btn-danger" data-toggle="modal"
                                                data-target="#editData_Delete{{ $p->id_divisi }}">
                                                <i class="fa fa-trash" aria-hidden="true"></i></button>
                                            </center>
                                        </td>
                                    </tr>
                                    @endforeach


                                </tbody>
                            </table>
                        </div>
                    </div>

                        <div id="addData" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/divisi/insert" method="post" enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Tambah Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="nama_divisi">Nama Divisi</label>
                                            <input type="text" class="form-control" name="nama_divisi" value="{{ old('nama_divisi') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="kode_divisi">Kode Divisi</label>
                                            <input type="text" class="form-control" name="kode_divisi" value="{{ old('kode_divisi') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="lantai_divisi">Lantai Divisi</label>
                                            <select class="form-control" name="lantai_divisi" required="required">
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="file_attach">Logo Divisi</label>
                                                <div class="custom-file" id="file_attach">
                                                    <input type="file" class="custom-file-input" id="customFile" name="image_divisi">
                                                    <label class="custom-file-label" for="customFile">Pilih File</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="url_divisi">URL Divisi</label>
                                            <input type="text" class="form-control" name="url_divisi" value="{{ old('url_divisi') }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="no_urut_divisi">No. Urut Divisi</label>
                                            <input type="number" class="form-control" name="no_urut_divisi" value="{{ old('no_urut_divisi') }}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Tambah</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        @foreach($divisi as $u)
                        <div id="editData_Update{{ $u->id_divisi }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/divisi/update{{ $u->id_divisi }}" method="post" enctype="multipart/form-data">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Edit Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="nama_divisi">Nama Divisi</label>
                                            <input type="text" class="form-control" name="nama_divisi" value="{{ $u->nama_divisi }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="kode_divisi">Kode Divisi</label>
                                            <input type="text" class="form-control" name="kode_divisi" value="{{ $u->kode_divisi }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="lantai_divisi">Lantai Divisi</label>
                                            <select class="form-control" name="lantai_divisi" required="required">
                                                <option value="{{ $u->lantai_divisi }}">{{ $u->lantai_divisi }}</option>
                                                <option value="5">5</option>
                                                <option value="6">6</option>
                                                <option value="7">7</option>
                                                <option value="8">8</option>
                                                <option value="9">9</option>
                                                <option value="10">10</option>
                                                <option value="11">11</option>
                                                <option value="12">12</option>
                                                <option value="14">14</option>
                                                <option value="15">15</option>
                                                <option value="16">16</option>
                                                <option value="17">17</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <label for="file_attach">Logo Divisi</label>
                                                <div class="custom-file" id="file_attach">
                                                    <input type="file" class="custom-file-input" id="customFile" name="image_divisi">
                                                    <label class="custom-file-label" for="customFile">Pilih File</label>
                                                </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="url_divisi">URL Divisi</label>
                                            <input type="text" class="form-control" name="url_divisi" value="{{ $u->url_divisi }}">
                                        </div>
                                        <div class="form-group">
                                            <label for="no_urut_divisi">No. Urut Divisi</label>
                                            <input type="number" class="form-control" name="no_urut_divisi" value="{{ $u->no_urut_divisi }}">
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-success">Update</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        @foreach($divisi as $u)
                        <div id="editData_Delete{{ $u->id_divisi }}" class="modal fade" role="dialog">
                            <div class="modal-dialog modal-dialog-centered">
                                <!-- Modal content-->
                                <div class="modal-content">
                                    <form action="/masterdata/divisi/delete{{ $u->id_divisi }}" method="post">

                                    {{ csrf_field() }}

                                    <div class="modal-header">
                                        <h4 class="modal-title">Hapus Data</h4>
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="box-body">
                                            <input type="hidden" class="form-control" name="id_divisi" value="{{ $u->id_divisi }}">
                                            <p>Apakah Anda yakin akan menghapus data ini?</p>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
                                        <button type="submit" class="btn btn-danger">Hapus</button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        @endforeach
                </div>
            </div>
        </div>
    </div>
</div>

		@if (count($errors) > 0)
            <div class="alert alert-danger">
	            <ul>
		            @foreach ($errors->all() as $error)
		            	<li>{{ $error }}</li>
		            @endforeach
	            </ul>
            </div>
        @endif

    <script type="text/javascript">
		$(document).ready(function() {
            $('#table_id').DataTable( {
                "scrollX": true
            } );
        } );
	</script>
@endsection
