<!-- Bootstrap css -->
<link href="../plugins/bootstrap-4.3.1/css/bootstrap.min.css" rel="stylesheet" />

<!-- Style css -->
<link href="../css/style.css" rel="stylesheet" />
<link href="../css/skin-modes.css" rel="stylesheet" />

<!-- Font-awesome  css -->
<link href="../css/icons.css" rel="stylesheet"/>

<!--Horizontal Menu css-->
<link href="../plugins/horizontal-menu/horizontal-menu.css" rel="stylesheet" />

<!--Select2 css -->
<link href="../plugins/select2/select2.min.css" rel="stylesheet" />

<!-- Cookie css -->
<!-- <link href="../plugins/cookie/cookie.css" rel="stylesheet"> -->

<!-- Owl Theme css-->
<link href="../plugins/owl-carousel/owl.carousel.css" rel="stylesheet" />

<!-- Custom scroll bar css-->
<link href="../plugins/scroll-bar/jquery.mCustomScrollbar.css" rel="stylesheet" />

<!-- Pscroll bar css-->
<link href="../plugins/pscrollbar/pscrollbar.css" rel="stylesheet" />

<!-- Switcher css -->
<link  href="../switcher/css/switcher.css" rel="stylesheet" id="switcher-css" type="text/css" media="all"/>

<!-- Color Skin css -->
<link id="theme" rel="stylesheet" type="text/css" media="all" href="../color-skins/color6.css" />