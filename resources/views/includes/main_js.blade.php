<!--JQuery Sparkline js-->
<script src="../js/jquery.sparkline.min.js"></script>

<!-- Circle Progress js-->
<script src="../js/circle-progress.min.js"></script>

<!-- Star Rating js-->
<script src="../plugins/rating/jquery.rating-stars.js"></script>

<!--Counters js-->
<script src="../plugins/counters/counterup.min.js"></script>
<script src="../plugins/counters/waypoints.min.js"></script>
<script src="../plugins/counters/numeric-counter.js"></script>

<!--Owl Carousel js -->
<script src="../plugins/owl-carousel/owl.carousel.js"></script>

<!--Horizontal Menu js-->
<script src="../plugins/horizontal-menu/horizontal-menu.js"></script>

<!--JQuery TouchSwipe js-->
<script src="../js/jquery.touchSwipe.min.js"></script>

<!--Select2 js -->
<script src="../plugins/select2/select2.full.min.js"></script>
<script src="../js/select2.js"></script>

<!-- sticky js-->
<script src="../js/sticky.js"></script>

<!-- Pscrollbar js -->
<script src="../plugins/pscrollbar/pscrollbar.js"></script>
<script src="../plugins/pscrollbar/pscroll.js"></script>

<!-- Cookie js -->
<!-- <script src="../plugins/cookie/jquery.ihavecookies.js"></script>
<script src="../plugins/cookie/cookie.js"></script> -->

<!-- Custom scroll bar js-->
<script src="../plugins/scroll-bar/jquery.mCustomScrollbar.concat.min.js"></script>

<!-- Swipe js-->
<script src="../js/swipe.js"></script>

<!-- Scripts js-->
<script src="../js/owl-carousel.js"></script>

<!-- Custom js-->
<script src="../js/custom.js"></script>