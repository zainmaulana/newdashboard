--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.19
-- Dumped by pg_dump version 11.5

-- Started on 2021-05-18 11:23:49

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 187 (class 1259 OID 44384)
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO postgres;

--
-- TOC entry 186 (class 1259 OID 44382)
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO postgres;

--
-- TOC entry 2207 (class 0 OID 0)
-- Dependencies: 186
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- TOC entry 182 (class 1259 OID 44356)
-- Name: migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO postgres;

--
-- TOC entry 181 (class 1259 OID 44354)
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.migrations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO postgres;

--
-- TOC entry 2208 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- TOC entry 192 (class 1259 OID 44420)
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO postgres;

--
-- TOC entry 193 (class 1259 OID 44431)
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO postgres;

--
-- TOC entry 185 (class 1259 OID 44375)
-- Name: password_resets; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO postgres;

--
-- TOC entry 189 (class 1259 OID 44396)
-- Name: permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_divisi bigint,
    iframe_report text,
    judul_report text,
    nama_report text,
    desc_report text,
    no_urut_report integer
);


ALTER TABLE public.permissions OWNER TO postgres;

--
-- TOC entry 188 (class 1259 OID 44394)
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO postgres;

--
-- TOC entry 2209 (class 0 OID 0)
-- Dependencies: 188
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- TOC entry 194 (class 1259 OID 44442)
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO postgres;

--
-- TOC entry 191 (class 1259 OID 44409)
-- Name: roles; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO postgres;

--
-- TOC entry 190 (class 1259 OID 44407)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO postgres;

--
-- TOC entry 2210 (class 0 OID 0)
-- Dependencies: 190
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- TOC entry 196 (class 1259 OID 44459)
-- Name: tblm_divisi; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.tblm_divisi (
    id_divisi bigint NOT NULL,
    nama_divisi text,
    kode_divisi text,
    lantai_divisi integer,
    image_divisi text,
    url_divisi text,
    no_urut_divisi integer
);


ALTER TABLE public.tblm_divisi OWNER TO postgres;

--
-- TOC entry 195 (class 1259 OID 44457)
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.tblm_divisi_id_divisi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tblm_divisi_id_divisi_seq OWNER TO postgres;

--
-- TOC entry 2211 (class 0 OID 0)
-- Dependencies: 195
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.tblm_divisi_id_divisi_seq OWNED BY public.tblm_divisi.id_divisi;


--
-- TOC entry 184 (class 1259 OID 44364)
-- Name: users; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    id_role bigint,
    id_divisi bigint
);


ALTER TABLE public.users OWNER TO postgres;

--
-- TOC entry 183 (class 1259 OID 44362)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO postgres;

--
-- TOC entry 2212 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- TOC entry 2035 (class 2604 OID 44387)
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- TOC entry 2033 (class 2604 OID 44359)
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- TOC entry 2037 (class 2604 OID 44399)
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- TOC entry 2038 (class 2604 OID 44412)
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- TOC entry 2039 (class 2604 OID 44462)
-- Name: tblm_divisi id_divisi; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_divisi ALTER COLUMN id_divisi SET DEFAULT nextval('public.tblm_divisi_id_divisi_seq'::regclass);


--
-- TOC entry 2034 (class 2604 OID 44367)
-- Name: users id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- TOC entry 2191 (class 0 OID 44384)
-- Dependencies: 187
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.failed_jobs (id, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- TOC entry 2186 (class 0 OID 44356)
-- Dependencies: 182
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2014_10_12_000000_create_users_table	1
2	2014_10_12_100000_create_password_resets_table	1
3	2019_08_19_000000_create_failed_jobs_table	1
4	2021_05_11_061131_create_permission_tables	2
\.


--
-- TOC entry 2196 (class 0 OID 44420)
-- Dependencies: 192
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- TOC entry 2197 (class 0 OID 44431)
-- Dependencies: 193
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\User	1
2	App\\User	2
\.


--
-- TOC entry 2189 (class 0 OID 44375)
-- Dependencies: 185
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- TOC entry 2193 (class 0 OID 44396)
-- Dependencies: 189
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.permissions (id, name, guard_name, created_at, updated_at, id_divisi, iframe_report, judul_report, nama_report, desc_report, no_urut_report) FROM stdin;
11	hkr	web	2021-05-17 17:02:28	2021-05-17 17:03:10	11	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
1	konsol_main	web	2021-05-11 17:05:00	2021-05-17 14:45:40	2	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Main View	Dashboard untuk Konsolidasi	1
4	konsol_lq	web	2021-05-17 14:17:17	2021-05-17 14:45:52	2	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard Liquidity	Liquidity Main View	Dashboard untuk Liquidity	2
2	dsu	web	2021-05-17 13:30:21	2021-05-17 15:45:42	3	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
3	epc	web	2021-05-17 13:38:27	2021-05-17 15:45:49	4	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
5	dg	web	2021-05-17 15:46:09	2021-05-17 15:46:25	5	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
6	pjt	web	2021-05-17 15:50:39	2021-05-17 17:01:07	6	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
7	opt	web	2021-05-17 17:01:16	2021-05-17 17:01:34	7	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
8	rjt	web	2021-05-17 17:01:47	2021-05-17 17:02:02	8	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
9	hki	web	2021-05-17 17:02:17	2021-05-17 17:02:44	9	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
10	hka	web	2021-05-17 17:02:21	2021-05-17 17:02:57	10	https://app.powerbi.com/view?r=eyJrIjoiYWRhZGYwY2UtMDE3MC00MTYyLWFhNGItOWU4ZjAyMTM4NjIyIiwidCI6Ijc1ZWFhYzQzLTlhNTUtNGM5Yy1iNGNjLTNjYTg1YjQxMzMxNyIsImMiOjEwfQ%3D%3D&pageName=ReportSectiond7a7c2d3abd8b7b54d69	3M21 Dashboard	Proyek	\N	1
\.


--
-- TOC entry 2198 (class 0 OID 44442)
-- Dependencies: 194
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
1	2
5	1
2	1
3	1
10	1
9	1
11	1
4	1
1	1
7	1
6	1
8	1
\.


--
-- TOC entry 2195 (class 0 OID 44409)
-- Dependencies: 191
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	Super Admin	web	2021-05-11 17:52:54	2021-05-11 17:52:54
2	Direksi	web	2021-05-17 13:37:15	2021-05-17 13:37:28
\.


--
-- TOC entry 2200 (class 0 OID 44459)
-- Dependencies: 196
-- Data for Name: tblm_divisi; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.tblm_divisi (id_divisi, nama_divisi, kode_divisi, lantai_divisi, image_divisi, url_divisi, no_urut_divisi) FROM stdin;
1	Sistem, TI & Riset Teknologi	SIT	8	Sistem, TI & Riset Teknologi_hk_logo.png	\N	\N
3	Sipil Umum	DSU	14	Sipil Umum_hk_dsu.png	/dashboard/dsu	2
5	Gedung	DG	11	Gedung_hk_gedung.png	/dashboard/dg	4
4	EPC	EPC	12	EPC_hk_epc.png	/dashboard/epc	3
6	PJT	PJT	10	PJT_hk_pjt.png	/dashboard/pjt	5
7	OPT	OPT	10	OPT_hk_pjt.png	/dashboard/opt	6
8	RJT	RJT	9	RJT_hk_pjt.png	/dashboard/rjt	7
9	HK Infrastruktur	HKI	15	HK Infrastruktur_hki.png	/dashboard/hki	8
10	Hakaaston	HKA	16	Hakaaston_hka_2.png	/dashboard/hka	9
11	HK Realtindo	HKR	17	HK Realtindo_hkr.png	/dashboard/hkr	10
2	Hutama Karya	KONSOL	6	Konsolidasi_hk_logo.png	/	1
12	Bhirawa Steel	BHIRAWA	9	Bhirawa Steel_hk_bhirawa.png	/dashboard/bhirawa	11
13	Semen Indogreen Sentosa	SIS	16	Semen Indogreen Sentosa_hk_sis.png	/dashboard/sis	12
\.


--
-- TOC entry 2188 (class 0 OID 44364)
-- Dependencies: 184
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.users (id, name, email, email_verified_at, password, remember_token, created_at, updated_at, id_role, id_divisi) FROM stdin;
1	Admin Dashboard HK	dashboard.hk@hutamakarya.com	\N	$2y$10$rRkAIMHRM35FR2RqyqNqmeIRs8uzh3Lv7/ZaPXaEg9twAFTQ9HHJe	\N	2021-05-11 05:55:02	2021-05-11 17:54:34	1	1
2	Admin SIT	admin.sit@hutamakarya.com	\N	$2y$10$pZSNl9UivGrLLka3UEvmb.aU8s7oRvpdHq5htO8VUQcNryItWSbK.	\N	2021-05-17 17:06:46	2021-05-17 17:06:46	2	1
\.


--
-- TOC entry 2213 (class 0 OID 0)
-- Dependencies: 186
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- TOC entry 2214 (class 0 OID 0)
-- Dependencies: 181
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.migrations_id_seq', 4, true);


--
-- TOC entry 2215 (class 0 OID 0)
-- Dependencies: 188
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.permissions_id_seq', 11, true);


--
-- TOC entry 2216 (class 0 OID 0)
-- Dependencies: 190
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.roles_id_seq', 2, true);


--
-- TOC entry 2217 (class 0 OID 0)
-- Dependencies: 195
-- Name: tblm_divisi_id_divisi_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.tblm_divisi_id_divisi_seq', 13, true);


--
-- TOC entry 2218 (class 0 OID 0)
-- Dependencies: 183
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.users_id_seq', 2, true);


--
-- TOC entry 2048 (class 2606 OID 44393)
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- TOC entry 2041 (class 2606 OID 44361)
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- TOC entry 2059 (class 2606 OID 44430)
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- TOC entry 2062 (class 2606 OID 44441)
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- TOC entry 2050 (class 2606 OID 44406)
-- Name: permissions permissions_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_name_guard_name_unique UNIQUE (name, guard_name);


--
-- TOC entry 2052 (class 2606 OID 44404)
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- TOC entry 2064 (class 2606 OID 44456)
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- TOC entry 2054 (class 2606 OID 44419)
-- Name: roles roles_name_guard_name_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_name_guard_name_unique UNIQUE (name, guard_name);


--
-- TOC entry 2056 (class 2606 OID 44417)
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2066 (class 2606 OID 44467)
-- Name: tblm_divisi tblm_divisi_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.tblm_divisi
    ADD CONSTRAINT tblm_divisi_pkey PRIMARY KEY (id_divisi);


--
-- TOC entry 2043 (class 2606 OID 44374)
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- TOC entry 2045 (class 2606 OID 44372)
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- TOC entry 2057 (class 1259 OID 44423)
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- TOC entry 2060 (class 1259 OID 44434)
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- TOC entry 2046 (class 1259 OID 44381)
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- TOC entry 2067 (class 2606 OID 44424)
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2068 (class 2606 OID 44435)
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2069 (class 2606 OID 44445)
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- TOC entry 2070 (class 2606 OID 44450)
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- TOC entry 2206 (class 0 OID 0)
-- Dependencies: 6
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2021-05-18 11:23:49

--
-- PostgreSQL database dump complete
--

