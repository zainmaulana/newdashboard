<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PermissionCustom extends Model
{
    //
    protected $table = "permissions";

    protected $primaryKey = "id";

    protected $fillable = ['name',
    'guard_name',
    'id_divisi',
    'iframe_report',
    'judul_report',
    'nama_report',
    'desc_report',
    'no_urut_report'];
}
