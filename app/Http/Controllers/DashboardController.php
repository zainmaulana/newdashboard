<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Divisi;
use App\PermissionCustom;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

use Carbon\Carbon;

class DashboardController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getTime(){
        /* set default timezone */
        date_default_timezone_set("Asia/Jakarta");
        $Hour = date('G');
        if( $Hour >= 1 && $Hour <= 11 ) {
          $greet = "Good Morning";
        }else if( $Hour >= 12 && $Hour <= 18 ){
            $greet = "Good Afternoon";
        }else if( $Hour >= 19 && $Hour <= 24){
            $greet = "Good Evening";
        }else{
            $greet = "Good Morning";
        }

        return $greet;
    }

    public function getUserDivisi(){

        $iduser = Auth::user()->id;
        $kodec = 'CCTV';

        //Daftar Divisi
        $userdivisi = DB::table('role_has_permissions as a')
        ->join('roles as b','a.role_id','=','b.id')
        ->join('permissions as c','a.permission_id','=','c.id')
        ->join('users as d','b.id','=','d.id_role')
        ->leftJoin('tblm_divisi as e','c.id_divisi','=','e.id_divisi')
        ->select('a.role_id',
        'a.permission_id',
        'b.name as nama_role',
        'c.name as nama_permission',
        'd.name as nama_user',
        'e.nama_divisi',
        'e.kode_divisi',
        'e.id_divisi',
        'e.image_divisi',
        'e.url_divisi')
        ->where('d.id','=',$iduser)
        ->whereRaw('substring(e.kode_divisi for 4) != ?',$kodec)
        ->distinct('e.no_urut_divisi')
        ->orderBy('e.no_urut_divisi','ASC')
        ->get();

        return $userdivisi;
    }

    public function getUserDivisiCCTV(){

        $iduser = Auth::user()->id;
        $kodec = 'CCTV';

        //Daftar Divisi
        $userdivisiCCTV = DB::table('role_has_permissions as a')
        ->join('roles as b','a.role_id','=','b.id')
        ->join('permissions as c','a.permission_id','=','c.id')
        ->join('users as d','b.id','=','d.id_role')
        ->leftJoin('tblm_divisi as e','c.id_divisi','=','e.id_divisi')
        ->select('a.role_id',
        'a.permission_id',
        'b.name as nama_role',
        'c.name as nama_permission',
        'd.name as nama_user',
        'e.nama_divisi',
        'e.kode_divisi',
        'e.id_divisi',
        'e.image_divisi',
        'e.url_divisi')
        ->where('d.id','=',$iduser)
        ->whereRaw('substring(e.kode_divisi for 4) = ?',$kodec)
        ->distinct('e.no_urut_divisi')
        ->orderBy('e.no_urut_divisi','ASC')
        ->get();

        return $userdivisiCCTV;
    }

    public function getListDashboard($kode_divisi){

        $iduser = Auth::user()->id;

        //Daftar Dashboard
        $listdashboard = DB::table('role_has_permissions as a')
        ->join('roles as b','a.role_id','=','b.id')
        ->join('permissions as c','a.permission_id','=','c.id')
        ->join('users as d','b.id','=','d.id_role')
        ->leftJoin('tblm_divisi as e','c.id_divisi','=','e.id_divisi')
        ->select('a.role_id',
        'a.permission_id',
        'c.name as nama_permission',
        'c.iframe_report',
        'c.judul_report',
        'c.nama_report',
        'c.desc_report',
        'd.name as nama_user',
        'e.nama_divisi',
        'e.kode_divisi')
        ->where([
            ['e.kode_divisi','=',$kode_divisi],
            ['d.id','=',$iduser]
            ])
        ->orderBy('c.no_urut_report','ASC')
        ->get();

        return $listdashboard;
    }

    public function getOneDashboard($kode_divisi){

        $iduser = Auth::user()->id;

        //Daftar Dashboard
        $onedashboard = DB::table('role_has_permissions as a')
        ->join('roles as b','a.role_id','=','b.id')
        ->join('permissions as c','a.permission_id','=','c.id')
        ->join('users as d','b.id','=','d.id_role')
        ->leftJoin('tblm_divisi as e','c.id_divisi','=','e.id_divisi')
        ->select('a.role_id',
        'a.permission_id',
        'c.name as nama_permission',
        'c.iframe_report')
        ->where([
            ['e.kode_divisi','=',$kode_divisi],
            ['d.id','=',$iduser]
            ])
        ->orderBy('c.no_urut_report','ASC')
        ->first()->iframe_report;

        return $onedashboard;
    }

    public function getListCCTV($kode_divisi){

        $iduser = Auth::user()->id;
        $kodec = 'cctv';

        //Daftar Dashboard
        $listCCTV = DB::table('role_has_permissions as a')
        ->join('roles as b','a.role_id','=','b.id')
        ->join('permissions as c','a.permission_id','=','c.id')
        ->join('users as d','b.id','=','d.id_role')
        ->leftJoin('tblm_divisi as e','c.id_divisi','=','e.id_divisi')
        ->select('a.role_id',
        'a.permission_id',
        'c.name as nama_permission',
        'c.iframe_report',
        'c.judul_report',
        'c.nama_report',
        'c.desc_report',
        'd.name as nama_user',
        'e.nama_divisi',
        'e.kode_divisi')
        ->where([
            ['e.kode_divisi','=',$kode_divisi],
            ['d.id','=',$iduser]
            ])
        ->whereRaw('substring(c.name for 4) = ?',$kodec)
        ->orderBy('c.no_urut_report','ASC')
        ->get();

        return $listCCTV;
    }

    public function index(){

        $kode_div = 'KONSOL';
        $judul = 'Dashboard Financial - Konsolidasi';

        $userdivisi = $this->getUserDivisi();
        $listdashboard = $this->getListDashboard($kode_div);
        $greet = $this->getTime();

        $routename = "/".Route::getFacadeRoot()->current()->uri();

        //Cek Password
        $user = Auth::user();
        $userPassword = $user->password;

        if(Hash::check('Hutama123!',$userPassword)){
            $resultCheckPass = 'true';
        }
        else{
            $resultCheckPass = 'false';
        }

        return view('dashboard', compact('userdivisi','listdashboard','judul','greet','routename','user','resultCheckPass'));
    }

    public function cctv_index(){

        $kode_div = 'CCTV_KONSOL';
        $judul = 'CCTV Proyek';

        $userdivisiCCTV = $this->getUserDivisiCCTV();
        $listCCTV = $this->getListCCTV($kode_div);
        $greet = $this->getTime();

        $routename = "/".Route::getFacadeRoot()->current()->uri();

        return view('cctv_home', compact('userdivisiCCTV','listCCTV','judul','greet','routename'));
    }

    public function cctv_auto(){

		$routename = "/".Route::getFacadeRoot()->current()->uri();
		$kode_divisi = DB::table('tblm_divisi')->where('url_divisi','=',$routename)->value('kode_divisi');
		$nama_divisi = DB::table('tblm_divisi')->where('url_divisi','=',$routename)->value('nama_divisi');

        $nama_divisi = explode('_',$nama_divisi);

        $judul = 'CCTV Proyek - '.$nama_divisi[1];

        $userdivisiCCTV = $this->getUserDivisiCCTV();
        $listCCTV = $this->getListCCTV($kode_divisi);
        $greet = $this->getTime();

        return view('cctv_home', compact('userdivisiCCTV','listCCTV','judul','greet','routename'));
    }

	public function dashboard_auto(){

		$routename = "/".Route::getFacadeRoot()->current()->uri();
		$kode_divisi = DB::table('tblm_divisi')->where('url_divisi','=',$routename)->value('kode_divisi');
		$nama_divisi = DB::table('tblm_divisi')->where('url_divisi','=',$routename)->value('nama_divisi');

		if($kode_divisi == 'OTHERS'){
            $judul = 'Dashboard - '.$nama_divisi;
        }
        elseif($kode_divisi == 'RAPAT'){
            $judul = $nama_divisi;
        }
        else{
            $judul = 'Dashboard Financial - '.$nama_divisi;
        }

        $userdivisi = $this->getUserDivisi();
        $listdashboard = $this->getListDashboard($kode_divisi);
        $greet = $this->getTime();

        //Cek Password
        $user = Auth::user();
        $userPassword = $user->password;

        if(Hash::check('Hutama123!',$userPassword)){
            $resultCheckPass = 'true';
        }
        else{
            $resultCheckPass = 'false';
        }

        return view('dashboard', compact('userdivisi','listdashboard','judul','greet','routename','user','resultCheckPass'));
    }

    public function dashboard_auto_url(){

		$routename = "/".Route::getFacadeRoot()->current()->uri();
		$kode_divisi = DB::table('tblm_divisi')->where('url_divisi','=',$routename)->value('kode_divisi');

        $onedashboard = $this->getOneDashboard($kode_divisi);

        return redirect($onedashboard);
    }

    public function change_pass_user(Request $data){

        //Validate Request
        $this->validate($data,[
            'password' => 'same:confirm-password',
        ]);

        //Update Request
        $user = User::where('email','=',$data->email)
        ->first();

        if($data->password <> NULL){
            $user->password = Hash::make($data->password);
        }
        elseif($data->password == NULL || $data->password == ''){

        }

        $user->save();

        return redirect('/');
    }
}
