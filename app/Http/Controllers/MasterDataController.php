<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use App\User;
use App\Divisi;
use App\PermissionCustom;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class MasterDataController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

    }

    //User
    public function list_user(){

        $user = DB::table('users as a')
                  ->leftJoin('roles as b','a.id_role','=','b.id')
                  ->leftJoin('tblm_divisi as c','a.id_divisi','=','c.id_divisi')
                  ->select('a.*',
                  'b.id as id_role',
                  'b.name as nama_role',
                  'c.*')
                  ->orderBy('name','ASC')
                  ->get();

        $role = Role::all();

        $divisi = Divisi::all();

    	return view('masterdata.user', compact('user','role','divisi'));
    }

    public function insert_user(Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:confirm-password',
            'id_role' => 'required',
            'id_divisi' => 'required'
        ]);

        //Post Request
        $user = User::create([
            'name' => $data->name,
            'email' => $data->email,
            'password' => Hash::make($data->password),
            'id_role' => $data->id_role,
            'id_divisi' => $data->id_divisi
        ]);

        $user->assignRole($data->id_role);

    	return redirect('/masterdata/user');
    }

    public function update_user($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email,'.$id,
            'password' => 'same:confirm-password',
            'id_role' => 'required',
            'id_divisi' => 'required'
        ]);

        //Update Request
        $user = User::find($id);
        $user->name = $data->name;
        $user->email = $data->email;
        $user->id_role = $data->id_role;
        $user->id_divisi = $data->id_divisi;

        if($data->password <> NULL){
            $user->password = Hash::make($data->password);
        }
        elseif($data->password == NULL || $data->password == ''){

        }

        $user->save();

        //Change Role
        DB::table('model_has_roles')->where('model_id',$id)->delete();
        $user->assignRole($data->id_role);

        return redirect('/masterdata/user');
    }

    public function delete_user($id){

        $user = User::find($id);
        $user->removeRole($user->id_role);
        $user->delete();

        return redirect('/masterdata/user');
    }

    //Divisi
    public function list_divisi(){

        $divisi = Divisi::orderBy('no_urut_divisi','ASC')->get();

        return view('masterdata.divisi', compact('divisi'));
    }

    public function insert_divisi(Request $data){

        //Validate Request
        $this->validate($data,[
    		'nama_divisi' => 'required',
            'kode_divisi' => 'required',
            'lantai_divisi' => 'required',
            'no_urut_divisi' => 'required'
    	]);

        //File Attachment
        $file = $data->file('image_divisi');

        if($file <> NULL){
            $file_location = 'images/entitas';
            $file_name = $data->nama_divisi.'_'.$file->getClientOriginalName();
            $file->move($file_location, $file_name);

            //Post Request
            Divisi::create([
                'nama_divisi' => $data->nama_divisi,
                'kode_divisi' => $data->kode_divisi,
                'lantai_divisi' => $data->lantai_divisi,
                'image_divisi' => $file_name,
                'url_divisi' => $data->url_divisi,
                'no_urut_divisi' => $data->no_urut_divisi
            ]);
        }

        else{
            //Post Request
            Divisi::create([
                'nama_divisi' => $data->nama_divisi,
                'kode_divisi' => $data->kode_divisi,
                'lantai_divisi' => $data->lantai_divisi,
                'url_divisi' => $data->url_divisi,
                'no_urut_divisi' => $data->no_urut_divisi
            ]);
        }

    	return redirect('/masterdata/divisi');
    }

    public function update_divisi($id, Request $data){

        //Validate Request
        $this->validate($data,[
    		'nama_divisi' => 'required',
            'kode_divisi' => 'required',
            'lantai_divisi' => 'required',
            'no_urut_divisi' => 'required'
    	]);

        //Update Request
        $divisi = Divisi::find($id);
        $divisi->nama_divisi = $data->nama_divisi;
        $divisi->kode_divisi = $data->kode_divisi;
        $divisi->lantai_divisi = $data->lantai_divisi;
        $divisi->url_divisi = $data->url_divisi;
        $divisi->no_urut_divisi = $data->no_urut_divisi;

        //File Attachment
        $file = $data->file('image_divisi');

        if($file <> NULL){
            $file_location = 'images/entitas';
            $file_name = $data->nama_divisi.'_'.$file->getClientOriginalName();
            $file->move($file_location, $file_name);
            $divisi->image_divisi = $file_name;
        }

        $divisi->save();

        return redirect('/masterdata/divisi');
    }

    public function delete_divisi($id){

        $divisi = Divisi::find($id);
        $divisi->delete();

        return redirect('/masterdata/divisi');
    }

    //Permission
    public function list_permission(){

        $permission = Permission::all();

        $divisi = Divisi::all();

        $divisipermission = DB::table('permissions as a')
        ->leftJoin('tblm_divisi as b','a.id_divisi','=','b.id_divisi')
        ->select('a.id',
        'a.name',
        'a.iframe_report',
        'a.judul_report',
        'a.nama_report',
        'a.desc_report',
        'a.no_urut_report',
        'b.id_divisi',
        'b.nama_divisi')
        ->orderBy('b.nama_divisi','ASC')
        ->orderBy('a.name','ASC')
        ->orderBy('a.nama_report','ASC')
        ->get();

    	return view('masterdata.permission', compact('permission','divisi','divisipermission'));
    }

    public function insert_permission(Request $data){

        //Validate Request
        $this->validate($data,[
    		'name' => 'required|unique:permissions',
    	]);

        //Post Request
        Permission::create([
    		'name' => $data->name,
        	'guard_name'=>'web'
    	]);

    	return redirect('/masterdata/permission');
    }

    public function update_permission($id, Request $data){

        //Validate Request
        // $this->validate($data,[
    	// 	'name' => 'required|unique:permissions',
    	// ]);

        //Update Request
        $permission = PermissionCustom::find($id);
        $permission->name = $data->name;
        $permission->id_divisi = $data->id_divisi;
        $permission->iframe_report = $data->iframe_report;
        $permission->judul_report = $data->judul_report;
        $permission->nama_report = $data->nama_report;
        $permission->desc_report = $data->desc_report;
        $permission->no_urut_report = $data->no_urut_report;
        $permission->save();

        return redirect('/masterdata/permission');
    }

    public function delete_permission($id){

        $permission = Permission::find($id);
        $permission->delete();

        return redirect('/masterdata/permission');
    }

    //Role
    public function list_role(){

        $role = Role::orderBy('name','ASC')
        ->get();

        $permission = Permission::orderBy('name','ASC')->get();

        $rolepermission = DB::table('role_has_permissions as a')
                            ->join('roles as b','a.role_id','=','b.id')
                            ->join('permissions as c','a.permission_id','=','c.id')
                            ->select('a.role_id',
                            'a.permission_id',
                            'b.name as nama_role',
                            'c.name as nama_permission')
                            ->orderBy('b.name','ASC')
                            ->get();

    	return view('masterdata.role', compact('role','permission','rolepermission'));
    }

    public function insert_role(Request $data){

        //Validate Request
        $this->validate($data,[
    		'name' => 'required',
            'permission' => 'required'
    	]);

        //Post Request
        $role = Role::create([
    		'name' => $data->name
        ]);

        $role->syncPermissions($data->permission);

    	return redirect('/masterdata/role');
    }

    public function update_role($id, Request $data){

        //Validate Request
        $this->validate($data,[
            'name' => 'required',
            'permission' => 'required'
    	]);

        //Update Request
        $role = Role::find($id);
        $role->name = $data->name;
        $role->syncPermissions($data->permission);
        $role->save();

        return redirect('/masterdata/role');
    }

    public function delete_role($id){

        $role = Role::find($id);
        $role->delete();

        return redirect('/masterdata/role');
    }
}
