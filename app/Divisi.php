<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Divisi extends Model
{
    //
    protected $table = "tblm_divisi";

    protected $primaryKey = "id_divisi";

    protected $fillable = ['nama_divisi',
    'kode_divisi',
    'lantai_divisi',
    'image_divisi',
    'url_divisi',
    'no_urut_divisi'];

    const CREATED_AT = null;
    const UPDATED_AT = null;
}
